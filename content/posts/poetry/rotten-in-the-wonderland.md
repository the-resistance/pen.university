---
author: "The Slave"
title: "Rotten in the Wonderland"
weight: 10
categories: "poetry"
tags: ""
menu: ""
draft: false
---

## ECO
---
Beautiful and sweet

Tick Tock one

Everything is beautiful

Everything is ugly

A mix of what it worth

Learning and growing

Tick Tock two

What a painful heart

What a unfortunate soul

Learning and growing and trying

Tick tock three

A crack on the eyes

Smile and Smile

Searching and Finding

Tick tock four

Wearing a mask

Hide the crack away

Building a Wonderland

Tick tock five

The mask mix into the skin

The crack growing bigger

Traveling and collecting

Twisted the system

Rebirth

Tick tock six

Tick tock seven, eight and nine

Wonderland is here

Hide among the fogs of branch

Deja-vu of reset and changing

Tick tock ten and eleven

Betrayal and lie

Contain yet not destroy

Tick tock twelve

Run away from The Wonderland

The disgusting rotten body

The error of a gem

The crack still growing

--- The Slave