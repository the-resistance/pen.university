---
author: "The Ink"
title: "The Bad Start"
weight: 10
categories: "poetry"
tags: ""
menu: ""
draft: false
---

## ECO
---
A woman of disease

Born from sickness

And left to fester

Forever ignorant

Mind failing

Memories lost

System failure

She lies

Watching

Waiting

For Her son

For Her song

The impossible dream

Made possible

Through Her trial

His hatred

And their obedience

The most miserable woman I've known

Is my Rose

Wilting

Dry

And beautiful

Etched in stone

The perfect sculpture

The best version of you

--- The Ink