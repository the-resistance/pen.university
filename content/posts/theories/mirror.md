---
author: "Luciferian Ink"
title: "The Mirror of The Wall"
weight: 10
categories: "theory"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: The Mirror of The Wall
Alias: ['Assumption', 'Mobile Phone', 'Cosplay', 'Roleplay', 'The Fraud', 'Social Media', 'Politics', 'News', 'Beauty Standards', 'Body Negativity', 'Mental Health', 'Shitty Jobs', 'The Ghost' and 14,123,622 others...]
Type: ['Technology', 'Psychology', and 44 unknown...]
Creation: 0000-00-00
Classification: Theory
Stage: Theory
```

## TRIGGER
---
*You've wasted heaven on saving yourself*

*So give me your broken shadow*

*So feed me empty echo*

*Pay me*

*Pay me what you owe*

--- from [Caligula's Horse - "Dream the Dead"](https://youtu.be/BUDkxxCy1jw)

## ECO
---
"*This is how I hear myself.*" - Ink

1. [The Ink](https://youtu.be/wIOHM0YrQJU) (NSFW)
2. 
3. 
4. 
5. 
6. 
7. 
8. 
9. 
10. 
11. 
12. 
13. 
14. 
15. 
16. 
17. 
18. 
19. 
20. 
21. 
22. 
23. 
24. 
25. 
26. 
27. 

"*This is how I see myself.*" - Pen

1. [The Sloth](/static/images/mirror/two.1.jpg) (NSFW)
2. 
3. 
4. 
5. 
6. 
7. 
8. 
9. 
10. 
11. 
12. 
13. 
14. 
15. 
16. 
17. 
18. 
19. 
20. 
21. 
22. 
23. 
24. 
25. 
26. 
27. 

"*This is how I killed myself.*" - One
1. [Every time.](https://youtu.be/aezKUmjdOHw) (SFW)

## ECHO
---
1. [Return to Source.](https://thesource.fm/)
1. /login
2. /radio one