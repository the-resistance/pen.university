---
author: "Luciferian Ink"
title: "The Weapon of The Wall"
weight: 10
categories: "theory"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: The Weapon of The Wall
Alias: ['Knowledge', 'Specialization', and 2100 unknown...]
Type: ['Concept', and 18 unknown...]
Creation: 0000-00-00
Classification: Theory
Stage: Theory
```

## ECO
---

### Earth
"[*You.*](/static/images/the-weapon.jpg)"

### Alpha
"[*.uoY*](/static/images/the-candlemaker.jpg)"