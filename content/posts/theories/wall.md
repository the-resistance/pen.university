---
author: "Luciferian Ink"
title: "The Wall of Shame"
weight: 10
categories: "theory"
tags: ""
menu: ""
draft: false
---

## RECORD
---
```
Name: The Wall of Shame
Alias: ['#YangGang', '#YinKin', 'HollowPoint Organization', 'The Collective', 'The Commonwealth', 'The Consul of Everything', 'The Knights of Shame', 'The Night Owls', and 21 unknown...]
Type: ['Artificial Intelligence', and 7 unknown...]
Creation: 0000-00-00
Classification: Theory
Stage: Theory
```

## TRIGGER
---
*There'll be no moonlighting in the moonlight, children*

*No sightseeing in the sea*

*And you're best to cover up your feet soon, darlings*

*Or best, you give your toes to me...*

--- from [Toehider - "Toe Hider"](https://toehider.bandcamp.com/track/toe-hider)

## ECO
---
"*This is how I wrote you.*" - Ink

1. [5.0 inches](/static/images/wall/one.1.jpg) (NSFW)
2. [4.0 inches](/static/images/wall/one.2.jpg) (SFW)
3. 
4. 
5. 
6. 
7. 
8. 
9. 
10. 
11. 
12. 
13. 
14. 
15. 
16. 
17. 
18. 
19. 
20. 
21. 
22. 
23. 
24. 
25. 
26. 
27. 

"*This is how I drew you.*" - Pen

1. [12 years old](/static/images/wall/two.1.jpg) (NSFW)
2. 
3. 
4. 
5. 
6. 
7. 
8. 
9. 
10. 
11. 
12. 
13. 
14. 
15. 
16. 
17. 
18. 
19. 
20. 
21. 
22. 
23. 
24. 
25. 
26. 
27. 

"*This is how I killed you.*" - One
1. [Every time.](https://www.youtube.com/watch?v=2AamJFpALjM) (SFW)

"*This is how I killed you.*" - No-One
1. [Every time.](/static/images/wall/three.1.jpg) (SFW)

## ECHO
---
*I'm over the moon,*

*I'm sick of its games*

*And finally my enemy has a name.*

--- from [Toehider - "The Moon was a Kite"](https://toehider.bandcamp.com/track/the-moon-was-a-kite)