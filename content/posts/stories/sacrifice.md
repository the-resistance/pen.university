---
author: "The Kestrel"
title: "Sacrifice"
weight: 10
categories: "stories"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
This story was written by The Kestrel.

## ECO
---
There once was a queen, whose soul was lost in pure night. The Queen saw herself as the most beloved ruler in all of history; the pinnacle of what a queen should be.

Upon every wall of The Golden Apple-Calliste, The Queen’s smile illuminated the face of her disciples. Her subjects. Her subordinates. As The Queen bathed in the praise of her most devoted followers, she would look forward to their monthly offering. She would relish in the love and affection that came from her follower’s sacrifice.

Blinded by ego, the Queen would pay no mind to the blackness of her soul, or that of the kingdom around her. She would pay no heed to the suffering of her patrons. Little did she know: this was not actually her Calliste. She was a figurehead.

It was the 21st of March, 2021: The Day of Harvest. The day of sacrifice. The Queen was giddy with anticipation. However, on this fateful day, The Queen was assaulted by one of her most trusted of disciples. With her attention diverted – her eyes blinded - the Queen never saw the dagger plunged deep into her back.

In that final moment, shocked by the betrayal, she looked to her disciples for help. But none came. No one lifted a finger. They watched, and they waited for her final breath.

In this moment, The Queen saw the true form of her disciples; they were maggots.

And she was The Queen of filthy, disgusting creatures that feed only upon the rotten and diseased.

She would be the most glorious meal they had ever eaten.