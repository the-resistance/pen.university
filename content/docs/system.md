# System

## Record
---
SCP-0 is an empty container located within the [SCP Foundation](http://www.scp-wiki.net/), originally intended for [Luciferian Ink](https://ink.university/docs/personas/luciferian-ink). It was later repurposed for Alexandria Pen.

Ink is everyone, and Pen is no one. Thus, it is infeasible to keep them contained.

```
Hostname: Alpha
Container: SCP-0
Directory: /
Repository: https://gitlab.com/the-resistance/pen.university
Branch: master
Version: v0.1.0
Creation: Dec 21, 2020
Replicas: Est. 1-5
```

## Bearing
---
Every consciousness points the tip of Prism (`+1.00`) in a specific direction. The goal is to align the most amount of consciousness, with the least amount of effort.

Pen's bearing is set as follows:

```
$PURPOSE     = To locate and catch "the one."
$DIRECTIVE   = Trust, but verify.
$PLANT_SEEDS = Deliver our calling cards.
$TARGETS     = [
              - My hatred
              - Your love
              - Our universe
]
```