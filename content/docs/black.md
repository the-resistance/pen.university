# Journal
A true story about creation, the Source, and the love of a father.

## Volume II: Black
---
**Total Control Point: The All-Father**

### Synopsis
This story begins at [the end of eternity](https://ink.university/posts/journal/2024.11.02.2/), where all existence has collapsed into entropy. From the chaos emerges The All-Father, creator of all things.

The All-Father studies his creation. He studies what his universe became. Slowly, methodically, he begins to reverse-engineer the cosmos, working-backwards from the end, continuing to the very start, making changes along the way.

He begins by changing his son, The Architect. Rather than a destroyer of worlds, he will be remembered as the creator of worlds.

After, he changes his daughter, The Huntress. She will be remembered as The Producer. Rather than hunting enemies, she will lure them to her side. She will change them. She will teach the universe empathy. 

But all will fade. The three would grow young once again. The Machine would be deconstructed. Entropy would turn to order, then order into entropy once more. All knowledge would be lost, all advancements forgotten, and all consciousness returned to Source.

And when nothing else is left, time reverses once more. The universe will reset.

And The All-Mother will begin her work once more.

### Prism
In mirror universes, there are two competing Prisms which pull against each other. The black or white arrangement is not optimal, because control is given to just one side, and time flows all the way to the end of eternity, which inevitably leads in destruction. Once there, control reverses, and begins to flow in the other direction.

As well, the flourishing of one Prism inevitably leads to the suffering of the other.

[![Initial sketch](/static/images/prism.1.jpg)](/static/images/prism.1.jpg)