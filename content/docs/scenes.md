# Scenes
## Overview
---
Scenes are the various places that a traveller may visit during his/her stay Hell. These places may be literal or fictional, or a combination of both.

## Scenes
---
- Hell
- Home
- Networks
- [Prism](https://ink.university/docs/scenes/prism/)
- The Door
- The Void