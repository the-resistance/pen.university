# Journal
A true story about love, empathy, and obsessive dedication to a cause.

## Volume I: Grey
---
**Transmission Control Point: The Pen**

### Synopsis
This story begins with The Raven, an old woman who lives in a [mirror universe where time flows backwards](https://www.pbs.org/wgbh/nova/article/big-bang-may-created-mirror-universe-time-runs-backwards/). She sits at the end of her life, reflecting upon the past that made her who she is. Reflecting upon the past that she has yet to experience, as she grows younger and younger over time.

Though they have never met, The Raven has been married to The Crow for her entire life. She and him are a model for a science experiment that is being used to pair predators with the people that they hurt, in an attempt to rehabilitate both. This man, in particular, will be remembered as the one who fixed this problem for both of their universes.

That is correct; the two do not live in the same universe. The two are juxtaposed in an important way. Where time in The Raven's universe flows backwards, time in The Crow's flows normally.

In this way, this becomes a tragic love story between an old woman and a young boy. We get to watch as the old woman grows younger, and the young boy grows older. The two lives converge, briefly, in an explosion of creativity surrounding 2020, before the story reverses: the man has grown old, and the woman has become a child once more. 

We watch The Crow fulfill his destiny, as we learn that The Raven was a product of his program from the very start. We get to experience The Raven return to childhood, and we get to see exactly what made her the beautiful old woman that she became.

In turn, we witness the Hell that The Crow came from, and experience the healing power that a dedicated life partner can bring upon a broken man.

[Though the two would never meet](https://ink.university/posts/journal/2024.11.03.0/), the love was real. 

And love conquers all.

### Prism
In mirror universes, there are two competing Prisms which pull against each other. The grey arrangement is optimal, because control of each Prism is shared by oscillating between each consciousness repeatedly. Thus, balance is maintained and the "end" of a Prism is never actually reached.

[![Initial sketch](/static/images/prism.0.jpg)](/static/images/prism.0.jpg)

When the Prisms pull too far apart, they will reflect, turning 180, moving inward. They will fly past each other in center, then ricochet off of the bottom of their reflection's Prism.

This is duality. This is the dual consciousness.