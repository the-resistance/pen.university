# The Bat
## RECORD
---
```
Name: Thomas Aldine
Alias: ['God', 'The Bat', 'The Dave', 'The Hunter', 'The President', 'The Rabbit King', and 80,853 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: Est. 45 Earth Years
Chronological Age: 6,431,780,348 Earth Years
SCAN Rank: | F F
           | F F
TIIN Rank: | F F
           | F F
Reviewer Rank: 1 stars
Organizations: 
  - The Corporation
  - The Resistance
Occupations:
  - Imposter
Variables:
  $CAPABILITY: -0.95 | # Totally ineffective.
```

## TRIGGER
---
Baseball.

## ECO
---
A liar in sheep's clothing.

## ECHO
---
*There are eight ways to solve every problem*

*And maybe one or two are good*

*There are five that look much more appealing than*

*Than the one you should*

--- from [Toehider - "Bats Aren't Birds"](https://toehider.bandcamp.com/track/bats-arent-birds)

## PREDICTION
---
```
The Bat will be awakened on October 31, 2021.
```