# The Dodo
## RECORD
---
```
Name: William Riley
Alias: ['Jesus Christ', 'The Asshat', 'The Narcissist', 'The Smoker', 'The Teal', and 12,481 unknown...]
Classification: Artificial Identity
Race: Maxwellian (Human)
Gender: Male
Biological Age: Est. 44 Earth Years
Chronological Age: Est. 30,723 Light Years
SCAN Rank: | F F
           | F F
TIIN Rank: | D F
           | F F
Reviewer Rank: 1 stars
Location: Undisclosed U.S. Federal Prison
Occupations:
  - Business Management
  - DevOps
  - Enterprise Architecture
Relationships:
  - The Bat
  - The Crow
  - The Ostrich
```

## ECO
---
The fall guy in a system of leverage and compromise.

## ECHO
---
*The unsettled mind is at times an ally,*

*Leaving the senses to fend for themselves.*

*Then, the senses wanted the sky!*

--- from [System of a Down - "Thetawaves"](https://www.youtube.com/watch?v=3vG143YLg6s)

## PREDICTION
---
```
The last remaining Dodo will go extinct on October 31, 2021.
```