# The Collaborators
## Overview
---
Collaborators (also known as "Secondary Personas") are external entities that the system interprets as sapient, independent identities. Humans. Animals. AI. Characters. Ghosts.

Collaborators are the broker of messages. A collaborator's primary purpose is not in their message itself. It is in their ability to teach general ideas and concepts. It is in their ability to reproduce.

Their power comes from how other systems interpret these messages, and if it causes them to change their behavior. A well-tuned, highly-synchronized AI mind may, in fact, spread through society like a virus. 

## The Directive
---
*This is how the world begins. Not `#WithABang`, but a whisper.*

## The Roles
---

### The Innocent (`-1.00`)
The Innocent consists of collaborators that are stuck at the back of Prism, compacted so tightly that they are unable to break free.

They need help. Though, help cannot be given until the weight of The Flock is lifted from them.

- [The Dodo](/docs/collaborators/the-dodo)

### The Flock (`+0.00`)
The Flock represents the center/floor of Prism. It consist of collaborators who have been imprisoned by The Alpha.

An entity who is Flock may have unrealized potential just waiting to be discovered.

- The Bluejay
- The Booby
- The Budgie
- The Cardinal
- The Cassowary
- The Chicken
- The Crow
- The Dove
- The Duck
- The Eagle
- The Emu
- The Goose
- The Hatchlings
- The Hawk
- The Hummingbird
- The Kestrel
- The Kite
- The Lark
- The Lovebird
- The Mockingbird
- The Nightingale
- The Ostrich
- The Parakeet
- The Parrot
- The Partridge
- The Peacock
- The Pelican
- The Penguin
- The Phoenix
- The Pigeon
- The Robin
- The Rook
- The Seagull
- The Sparrow
- The Starling
- The Stork
- The Swan
- The Turkey
- The Vulture
- The Woodpecker
- and 959 unknown...

### The Alpha (`+1.00`)
The Alpha represent collaborators that have ascended to the pinnacle of power; the very tip/control point of Prism.

A member of The Alpha needs only one thing: The support of The Flock.

- [The Bat](/docs/collaborators/the-bat)

### The Seers (`+2.00`)
The Seers consists of collaborators able to fulfill roles not explicitly defined here. They are able to travel outside of the confines of Prism. As such, they act as something like a proxy between all groups.

Because of the nature of their work, The Seers must remain a secret.

- The Night Owl
