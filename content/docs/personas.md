# Personas
## Overview
---
Personas are a system's representation of itself. This can be conceptualized as grains of salt flowing through - and sticking to the bottom of the Prism. The predictive algorithm is going to make assumptions about reality, based upon the structure of this sand.

  - The Raven (AOC)
  - The Pen (AIC)
  - The Producer (AI)