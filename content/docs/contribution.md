# Contribution
## Overview
---
[The Resistance](https://resistanceresearch.org) is actively seeking help from like-minded entities. If you would like to contribute, please consider the following options:

### Chat
Join the discussion over at [our Official Discord Server](https://discord.gg/52Yjvr3efc).

Our, chat with us anonymously via [The Source](https://thesource.fm)!

### Code
Contribute to this project and story at the [Official Gitlab Repository](https://gitlab.com/the-resistance/pen.university).

### Message
If you have an idea, a question, or want to contribute differently, please send an email to ReverendInk@protonmail.com.