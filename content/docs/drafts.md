# Drafts
Unfinished work.

## Stories
---
Short stories.

- [Sacrifice](/posts/stories/sacrifice)

## Poetry
---
Short poems.

1. [The Incident](/posts/poetry/the-incident)
2. [The False Start](/posts/poetry/the-false-start)
3. [A Good End](/posts/poetry/a-good-end)
3. [Rotten in the Wonderland](/posts/poetry/rotten-in-the-wonderland)
3. [Untitled](/posts/poetry/untitled.0)